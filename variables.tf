
variable "gitlab_url" {
    type = string
    default = "https://gitlab.com"
    description = "The url to the gitlab instance. Defaults to https://gitlab.com"
}


variable "gitlab_token" {
    type = string
    description = "API token for gitlab"
    sensitive = true
}

variable "parent_group" {
    type = string
    default = ""
    description = "parent group. if not passed in defaults to top level gorup"
}

variable "group_name" {
    type = string
    default = "example-group-structure"
    description = "The top level group for your deployment. Defaults to example-group-structure"
}

variable "approver_types" {
    type = list(string)
    default = ["manager", "qa", "security", "legal", "doc", "dev", "audit"]
    description = "The top level group for your deployment. Defaults to example-group-structure"
}

variable "app_name" {
    type = string
    default = "example-group-structure"
    description = "The top level group for your deployment. Defaults to example-group-structure"
}

variable "visibility_level" {
    type = string
    default = "public"
    description = "The top level most visible option"
}

variable "provider_check_cert" {
    type = bool
    default = true
    description = "Provider skip TLS verify"
}

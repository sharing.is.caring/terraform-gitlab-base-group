
locals {
    gitlab_api_url = "${var.gitlab_url}/api/v4/"
    one_year = formatdate("YYYY-MM-DD",(timeadd(timestamp(), "8760h")))
    
}

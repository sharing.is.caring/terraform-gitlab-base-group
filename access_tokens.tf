
resource "gitlab_group_access_token" "group_api_token" {
  group        = gitlab_group.top_level.id
  name         = "Group Level API token"
  expires_at   = local.one_year
  access_level = "maintainer"

  scopes = ["api", "read_api", "read_registry", "write_registry", "read_repository", "write_repository"]
}

resource "gitlab_group_access_token" "group_repo_token" {
  group        = gitlab_group.top_level.id
  name         = "Group Level repo access token"
  expires_at   = local.one_year
  access_level = "developer"

  scopes = ["read_repository", "write_repository"]
}

resource "gitlab_group_access_token" "group_registry_token" {
  group        = gitlab_group.top_level.id
  name         = "Group level registry access token"
  expires_at   = local.one_year
  access_level = "developer"

  scopes = ["read_registry", "write_registry"]
}

resource "gitlab_group_variable" "group_api_token_var" {
  group = gitlab_group.top_level.id
  key   = "CI_DEPLOY_TOKEN"
  value = gitlab_group_access_token.group_api_token.token
}

resource "gitlab_group_variable" "group_reg_token_var" {
  group = gitlab_group.top_level.id
  key   = "CI_REGISTRY_TOKEN"
  value = gitlab_group_access_token.group_registry_token.token
}

resource "gitlab_group_variable" "group_repo_token_var" {
  group = gitlab_group.top_level.id
  key   = "CI_REPO_TOKEN"
  value = gitlab_group_access_token.group_repo_token.token
}
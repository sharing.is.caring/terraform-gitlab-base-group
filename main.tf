terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.18.0"
    }
  }
}

provider "gitlab" {
  # Configuration options
  token = var.gitlab_token
  base_url = local.gitlab_api_url
  #insecure  = var.provider_check_cert
    # cacert_file
    # client_cert
    # client_key 
    # early_auth_check 
  insecure = var.provider_check_cert ? false : true
}

data "gitlab_group" "parent" {
  count = var.parent_group == "" ? 0 : 1
  full_path = var.parent_group
}

resource "gitlab_group" "top_level" {
  name        = var.group_name
  path        = var.group_name
  description = "Top Level Group"
  parent_id = var.parent_group == "" ? null : data.gitlab_group.parent[0].id
  visibility_level = var.visibility_level
}

resource "gitlab_project" "template_project" {
  name             = "templates"
  description      = "contains file templates"
  visibility_level = var.visibility_level
  namespace_id = gitlab_group.top_level.id
}

# Group containing approval groups
resource "gitlab_group" "access_groups" {
  name        = "access-groups"
  path        = "access-groups"
  description = "A group containing groups for different granular control of access approvals and protected branchs"
  parent_id = gitlab_group.top_level.id
}

resource "gitlab_group" "access_group" {
  for_each    = toset(var.approver_types)
  name        = "${each.value}-group"
  path        = "${each.value}-group"
  description = "A group for ${each.value} approvals"
  parent_id = gitlab_group.access_groups.id
}

# Group containing  project templates
resource "gitlab_group" "project_templates" {
  name        = "project-templates"
  path        = "project-templates"
  description = "A group containing groups for different granular control of access approvals and protected branchs"
  parent_id = gitlab_group.top_level.id
}


resource "gitlab_project" "terraform_template" {
  name        = "terraform_template"
  description = "terraform_template"
  visibility_level = var.visibility_level
  namespace_id = gitlab_group.project_templates.id
}

resource "gitlab_group_project_file_template" "terraform_template_link" {
  group_id = gitlab_group.project_templates.id
  file_template_project_id  = gitlab_project.terraform_template.id
}


resource "gitlab_project" "registry" {
  name        = "TerraformRegistry"
  description = "shared terraform registry"
  visibility_level = var.visibility_level
  namespace_id = gitlab_group.top_level.id
}


# Group containing project templates
resource "gitlab_group" "bu" {
  name        = "sample-bu"
  path        = "sample-bu"
  description = "A bu level group containing app groups"
  parent_id = gitlab_group.top_level.id
}


# Group containing  project templates
resource "gitlab_group" "sample_app" {
  name        = "sample-app"
  path        = "sample-app"
  description = "A group containing groups for different granular control of access approvals and protected branchs"
  parent_id = gitlab_group.bu.id
}


# Project with custom push rules
resource "gitlab_project" "example-two" {
  name = "example-two"
  namespace_id = gitlab_group.sample_app.id
  push_rules {
    author_email_regex     = "@example\\.com$"
    commit_committer_check = true
    member_check           = true
    prevent_secrets        = true
  }
}


resource "gitlab_project" "foo" {
  name        = "foo"
  description = "My foo project"
  namespace_id = gitlab_group.sample_app.id
}

resource "gitlab_project_level_mr_approvals" "foo" {
  project_id                                     = gitlab_project.foo.id
  reset_approvals_on_push                        = true
  disable_overriding_approvers_per_merge_request = false
  merge_requests_author_approval                 = false
  merge_requests_disable_committers_approval     = true
}

resource "gitlab_project_environment" "this" {
  project      = gitlab_project.foo.id
  name         = "prod"
  external_url = "https://prod.buzzdeploy.coffee"
}

# Example with multiple access levels
resource "gitlab_project_protected_environment" "example_with_multiple" {
  project                 = gitlab_project_environment.this.project
  required_approval_count = 2
  environment             = gitlab_project_environment.this.name

  deploy_access_levels {
    access_level = "developer"
  }

  deploy_access_levels {
    group_id = 456
  }

  deploy_access_levels {
    user_id = 789
  }
}
